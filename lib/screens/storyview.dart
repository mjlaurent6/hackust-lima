import 'dart:ffi';
import 'dart:async';
import 'package:flutter/material.dart';
import 'package:flutter_travel_ui/data/storydata.dart';
import '../data/storydata.dart';

class StoryView extends StatefulWidget {
  final StoryData story;
  StoryView({Key key, @required this.story}) : super(key: key);
  @override
  _StoryViewState createState() => _StoryViewState();
}

class _StoryViewState extends State<StoryView> {
  double percent = 0.0;
  Timer _timer;
  void startTimer() {
    _timer = Timer.periodic(Duration(microseconds: 10), (timer) {
      setState(() {
        percent += 0.00001;
        if (percent > 1) {
          _timer.cancel();
          Navigator.pop(context);
        }
      });
    });
  }

  @override
  void initState() {
    // TODO: implement initState
    startTimer();
    super.initState();
  }

  void backHandler() {
    Navigator.pop(context);
    print("Hello");
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        children: [
          Container(
            width: double.infinity,
            height: double.infinity,
            decoration: BoxDecoration(
              image: DecorationImage(
                image: AssetImage('assets/images/disneyland.png'),
                fit: BoxFit.cover,
              ),
            ),
          ),
          Container(
            padding: EdgeInsets.symmetric(vertical: 36.0, horizontal: 8.0),
            child: Container(
              child: LinearProgressIndicator(
                value: percent,
                valueColor: AlwaysStoppedAnimation<Color>(Colors.blue),
              ),
            ),
          ),
          // Container(
          //   alignment: Alignment.topLeft,
          //   padding: EdgeInsets.symmetric(vertical: 40.0, horizontal: 8.0),
          //   child: Row(
          //     children: [
          //       IconButton(
          //         icon: Icon(Icons.arrow_back),
          //         iconSize: 30.0,
          //         color: Colors.black,
          //         onPressed: () => Navigator.pop(context),
          //       ),
          //     ],
          //   ),
          // ),
          Padding(
            padding: EdgeInsets.symmetric(horizontal: 10.0, vertical: 40.0),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                IconButton(
                  icon: Icon(Icons.arrow_back),
                  iconSize: 30.0,
                  color: Colors.black,
                  onPressed: () {
                    Navigator.pushNamed(context, 'home');
                  },
                ),
              ],
            ),
          ),
          Container(
            decoration: BoxDecoration(
              image: DecorationImage(
                image: AssetImage("assets/images/story_bg.png"),
                fit: BoxFit.fitHeight,
                scale: 0.3,
              ),
            ),
            child: Padding(
                padding: EdgeInsets.symmetric(vertical: 30.0, horizontal: 30.0),
                child: Row(
                  children: [
                    Container(
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        mainAxisSize: MainAxisSize.max,
                        mainAxisAlignment: MainAxisAlignment.end,
                        children: [
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                            children: [
                              Column(
                                children: [
                                  CircleAvatar(
                                    backgroundImage:
                                        AssetImage('assets/images/anna.png'),
                                    radius: 30.0,
                                  ),
                                  SizedBox(
                                    width: 8.0,
                                  ),
                                ],
                              ),
                              Container(
                                padding:
                                    EdgeInsets.fromLTRB(20.0, 10.0, 20.0, 10.0),
                                child: Column(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: [
                                    Align(
                                      alignment: Alignment.centerLeft,
                                      child: Text(
                                        '@adriannaYick4-3',
                                        style: TextStyle(
                                          color: Colors.black,
                                          fontSize: 13.0,
                                          fontWeight: FontWeight.bold,
                                        ),
                                      ),
                                    ),
                                    Align(
                                      alignment: Alignment.centerLeft,
                                      child: Text(
                                        'Miss this feeling #disneyland',
                                        style: TextStyle(
                                          color: Colors.black,
                                          fontSize: 15.0,
                                        ),
                                      ),
                                    ),
                                    Align(
                                      alignment: Alignment.centerLeft,
                                      child: Text(
                                        'Hong Kong Disneyland',
                                        style: TextStyle(
                                          color: Colors.black,
                                          fontSize: 13.0,
                                        ),
                                      ),
                                    )
                                  ],
                                ),
                              ),
                              Column(
                                children: [
                                  Icon(
                                    Icons.favorite,
                                    color: Colors.black,
                                    size: 20.0,
                                  ),
                                  Icon(
                                    Icons.add_comment_outlined,
                                    color: Colors.black,
                                    size: 20.0,
                                  ),
                                  Icon(
                                    Icons.add_circle_outline,
                                    color: Colors.black,
                                    size: 20.0,
                                  ),
                                ],
                              )
                            ],
                          )
                        ],
                      ),
                    ),
                  ],
                )),
          ),
        ],
      ),
    );
  }
}
