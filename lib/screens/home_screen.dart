import 'package:flutter/material.dart';
import 'package:flutter_travel_ui/widgets/destination_carousel.dart';
import 'package:flutter_travel_ui/widgets/food_carousel.dart';
import 'package:flutter_travel_ui/widgets/hotel_carousel.dart';
import 'package:flutter_travel_ui/widgets/museum_carousel.dart';
import 'package:flutter_travel_ui/widgets/outdoor_carousel.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:flutter_travel_ui/components/storybtn.dart';
import 'package:flutter_travel_ui/data/storydata.dart';
import 'dart:async';

class HomeScreen extends StatefulWidget {
  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  List<StoryData> stories = [
    new StoryData(
        "Adrianna Yick",
        "https://images.unsplash.com/photo-1539571696357-5a69c17a67c6?ixid=MnwxMjA3fDB8MHxzZWFyY2h8MXx8cGVvcGxlfGVufDB8fDB8fA%3D%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=500&q=60",
        "https://images.unsplash.com/photo-1587808006954-2fe42637fda3?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=564&q=80"),
  ];
  int _currentTab = 0;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: ListView(
          padding: EdgeInsets.symmetric(vertical: 5.0),
          children: <Widget>[
            RichText(
              text: TextSpan(
                style: Theme.of(context).textTheme.body1,
                children: [
                  WidgetSpan(
                    child: Padding(
                      padding: EdgeInsets.only(left: 20.0, right: 120.0),
                      child: Text(
                        'Hong Kong',
                        style: TextStyle(
                          fontSize: 30.0,
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                    ),
                  ),
                  WidgetSpan(
                    child: Padding(
                      padding: const EdgeInsets.only(
                        left: 70.0,
                        bottom: 1,
                      ),
                      child: Icon(
                        Icons.search,
                        size: 35,
                      ),
                    ),
                  ),
                ],
              ),
            ),
            SizedBox(height: 20.0),
            Padding(
              padding: EdgeInsets.symmetric(horizontal: 12.0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  SizedBox(width: 10.0),
                  Text(
                    'For you ',
                    style: TextStyle(
                      fontSize: 14.0,
                      fontFamily: 'Montserrat',
                      letterSpacing: 1.5,
                      shadows: [
                        Shadow(color: Colors.black, offset: Offset(0, -5))
                      ],
                      color: Colors.transparent,
                      decoration: TextDecoration.underline,
                      decorationColor: Colors.blue,
                      decorationThickness: 2,
                    ),
                  ),
                  SizedBox(width: 2.5),
                  Text(
                    '| ',
                    style: TextStyle(
                      fontSize: 14.0,
                      fontFamily: 'Montserrat',
                      letterSpacing: 1.5,
                      shadows: [
                        Shadow(color: Colors.black, offset: Offset(0, -5))
                      ],
                      color: Colors.transparent,
                    ),
                  ),
                  SizedBox(width: 2.5),
                  Text(
                    'Hotels ',
                    style: TextStyle(
                      fontSize: 14.0,
                      fontFamily: 'Montserrat',
                      letterSpacing: 1.5,
                      shadows: [
                        Shadow(color: Colors.black, offset: Offset(0, -5))
                      ],
                      color: Colors.transparent,
                    ),
                  ),
                  SizedBox(width: 2.5),
                  Text(
                    '|',
                    style: TextStyle(
                      fontSize: 14.0,
                      fontFamily: 'Montserrat',
                      letterSpacing: 1.5,
                      shadows: [
                        Shadow(color: Colors.black, offset: Offset(0, -5))
                      ],
                      color: Colors.transparent,
                    ),
                  ),
                  SizedBox(width: 2.5),
                  Text(
                    'Food ',
                    style: TextStyle(
                      fontSize: 14.0,
                      fontFamily: 'Montserrat',
                      letterSpacing: 1.5,
                      shadows: [
                        Shadow(color: Colors.black, offset: Offset(0, -5))
                      ],
                      color: Colors.transparent,
                    ),
                  ),
                  SizedBox(width: 2.5),
                  Text(
                    '| ',
                    style: TextStyle(
                      fontSize: 14.0,
                      fontFamily: 'Montserrat',
                      letterSpacing: 1.5,
                      shadows: [
                        Shadow(color: Colors.black, offset: Offset(0, -5))
                      ],
                      color: Colors.transparent,
                    ),
                  ),
                  SizedBox(width: 2.5),
                  Text(
                    'Outdoor ',
                    style: TextStyle(
                      fontSize: 14.0,
                      fontFamily: 'Montserrat',
                      letterSpacing: 1.5,
                      shadows: [
                        Shadow(color: Colors.black, offset: Offset(0, -5))
                      ],
                      color: Colors.transparent,
                    ),
                  ),
                  SizedBox(width: 2.5),
                  Text(
                    '| ',
                    style: TextStyle(
                      fontSize: 14.0,
                      fontFamily: 'Montserrat',
                      letterSpacing: 1.5,
                      shadows: [
                        Shadow(color: Colors.black, offset: Offset(0, -5))
                      ],
                      color: Colors.transparent,
                    ),
                  ),
                  SizedBox(width: 2.5),
                  Text(
                    'Museum',
                    style: TextStyle(
                      fontSize: 14.0,
                      fontFamily: 'Montserrat',
                      letterSpacing: 1.5,
                      shadows: [
                        Shadow(color: Colors.black, offset: Offset(0, -5))
                      ],
                      color: Colors.transparent,
                    ),
                  ),
                ],
              ),
            ),
            Container(
              padding: EdgeInsets.fromLTRB(30, 10, 25, 0),
              child: Text(
                'For you',
                style: TextStyle(
                  fontSize: 15.0,
                  fontWeight: FontWeight.bold,
                ),
              ),
            ),
            SizedBox(height: 5.0),
            DestinationCarousel(stories[0]),
            Container(
              padding: EdgeInsets.fromLTRB(30, 10, 25, 0),
              child: Text(
                'Hotel',
                style: TextStyle(
                  fontSize: 15.0,
                  fontWeight: FontWeight.bold,
                ),
              ),
            ),
            SizedBox(height: 5.0),
            HotelCarousel(),
            Container(
              padding: EdgeInsets.fromLTRB(30, 10, 25, 0),
              child: Text(
                'Food',
                style: TextStyle(
                  fontSize: 15.0,
                  fontWeight: FontWeight.bold,
                ),
              ),
            ),
            SizedBox(height: 5.0),
            FoodCarousel(),
            Container(
              padding: EdgeInsets.fromLTRB(30, 10, 25, 0),
              child: Text(
                'Outdoor',
                style: TextStyle(
                  fontSize: 15.0,
                  fontWeight: FontWeight.bold,
                ),
              ),
            ),
            SizedBox(height: 5.0),
            OutdoorCarousel(),
            Container(
              padding: EdgeInsets.fromLTRB(30, 10, 25, 0),
              child: Text(
                'Museum',
                style: TextStyle(
                  fontSize: 15.0,
                  fontWeight: FontWeight.bold,
                ),
              ),
            ),
            SizedBox(height: 5.0),
            MuseumCarousel(),
            SizedBox(height: 5.0),
          ],
        ),
      ),
      bottomNavigationBar: BottomNavigationBar(
        type: BottomNavigationBarType.fixed,
        currentIndex: _currentTab,
        onTap: (int value) {
          setState(() {
            _currentTab = value;
          });
        },
        items: [
          BottomNavigationBarItem(
            icon: Icon(
              Icons.location_pin,
              size: 30.0,
            ),
            title: SizedBox.shrink(),
          ),
          BottomNavigationBarItem(
            icon: Icon(
              Icons.add,
              size: 30.0,
            ),
            title: SizedBox.shrink(),
          ),
          BottomNavigationBarItem(
            icon: Icon(
              Icons.calendar_today_rounded,
              size: 30.0,
            ),
            title: SizedBox.shrink(),
          ),
          BottomNavigationBarItem(
            icon: Icon(
              Icons.person,
              size: 30.0,
            ),
            title: SizedBox.shrink(),
          )
        ],
      ),
    );
  }
}
