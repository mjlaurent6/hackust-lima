import 'package:flutter_travel_ui/models/activity_model.dart';

class Destination {
  String imageUrl;
  String place;
  String location;
  String views;
  List<Activity> activities;

  Destination({
    this.imageUrl,
    this.place,
    this.location,
    this.views,
  });
}

List<Destination> destinations = [
  Destination(
    imageUrl: 'assets/images/hk_disney.png',
    place: 'Hong Kong Disn..',
    location: 'Lantau Island',
    views: '15.6K',
  ),
  Destination(
    imageUrl: 'assets/images/cheung_chau.png',
    place: 'Cheung Chau Island',
    location: 'Island',
    views: '11.8K',
  ),
  Destination(
    imageUrl: 'assets/images/k11.png',
    place: 'K11 Musea',
    location: 'Tsim Sha Tsui',
    views: '10.3K',
  ),
  Destination(
    imageUrl: 'assets/images/mansons_lot.png',
    place: 'Mansons Lot Coffee',
    location: 'Wan Chai',
    views: '7.8K',
  ),
  Destination(
    imageUrl: 'assets/images/pottinger.jpeg',
    place: 'The Pottinger',
    location: 'Central',
    views: '8.5K',
  ),
];
