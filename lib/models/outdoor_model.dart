import 'package:flutter_travel_ui/models/activity_model.dart';

class Outdoor {
  String imageUrl;
  String place;
  String location;
  String views;

  Outdoor({
    this.imageUrl,
    this.place,
    this.location,
    this.views,
  });
}

List<Outdoor> outdoors = [
  Outdoor(
    imageUrl: 'assets/images/hk_disney.png',
    place: 'Hong Kong Disn..',
    location: 'Lantau Island',
    views: '15.6K',
  ),
  Outdoor(
    imageUrl: 'assets/images/cheung_chau.png',
    place: 'Cheung Chau Island',
    location: 'Island',
    views: '11.8K',
  ),
  Outdoor(
    imageUrl: 'assets/images/k11.png',
    place: 'K11 Musea',
    location: 'Tsim Sha Tsui',
    views: '10.3K',
  ),
  Outdoor(
    imageUrl: 'assets/images/victoria_peak.jpeg',
    place: 'Victoria Peak',
    location: 'Central',
    views: '16.2K',
  ),
  Outdoor(
    imageUrl: 'assets/images/lan_kwai_fong.jpeg',
    place: 'Lan Kwai Fong',
    location: 'Central',
    views: '20.4K',
  ),
];
