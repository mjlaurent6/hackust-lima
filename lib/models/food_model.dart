class Food {
  String imageUrl;
  String name;
  String address;

  Food({
    this.imageUrl,
    this.name,
    this.address,
  });
}

final List<Food> foods = [
  Food(
    imageUrl: 'assets/images/mansons_lot.png',
    name: 'Mansons Lot Coffee',
    address: '3 Swallow St Wan Chai',
  ),
  Food(
    imageUrl: 'assets/images/scarlett_cafe.jpeg',
    name: 'Scarlett Cafe & Wine Bar',
    address: '2 Austin Ave TST',
  ),
  Food(
    imageUrl: 'assets/images/riverside_grill.jpeg',
    name: 'Riverside Grill',
    address: '21 Tong Chun St TKO',
  ),
];
