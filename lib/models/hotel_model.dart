class Hotel {
  String imageUrl;
  String name;
  String address;
  int price;

  Hotel({
    this.imageUrl,
    this.name,
    this.address,
    this.price,
  });
}

final List<Hotel> hotels = [
  Hotel(
    imageUrl: 'assets/images/pottinger.jpeg',
    name: 'The Pottinger',
    address: '74 Queens Road Central',
    price: 1150,
  ),
  Hotel(
    imageUrl: 'assets/images/marco_polo.jpeg',
    name: 'Marco Polo TST',
    address: '3 Canton Road Tsim Sha Tsui',
    price: 750,
  ),
  Hotel(
    imageUrl: 'assets/images/crowne_plaza.jpeg',
    name: 'Crowne Plaza TKO',
    address: '3 Tong Tak St TKO',
    price: 580,
  ),
];
