class Museum {
  String imageUrl;
  String name;
  String address;
  int price;

  Museum({
    this.imageUrl,
    this.name,
    this.address,
    this.price,
  });
}

final List<Museum> museums = [
  Museum(
    imageUrl: 'assets/images/space_museum.jpeg',
    name: 'Space Museum',
    address: '10 Salisbury Rd TST',
    price: 34,
  ),
  Museum(
    imageUrl: 'assets/images/art_museum.jpeg',
    name: 'Museum of Art',
    address: '10 Salisbury Rd TST',
    price: 30,
  ),
  Museum(
    imageUrl: 'assets/images/heritage_museum.jpeg',
    name: 'Heritage Museum',
    address: '1 Man Lam Rd Sha Tin',
    price: 20,
  ),
];
