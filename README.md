# Travel UI for Xplore

hackUST 2021 Participants
Developed by:
- LAURENT, Michael Jhon
- DYCHENGBENG, Matthew Go


## Getting Started

This project is a starting point for a Flutter application.

A few resources to get you started if this is your first Flutter project:

- [Lab: Write your first Flutter app](https://flutter.dev/docs/get-started/codelab)
- [Cookbook: Useful Flutter samples](https://flutter.dev/docs/cookbook)

For help getting started with Flutter, view our
[online documentation](https://flutter.dev/docs), which offers tutorials,
samples, guidance on mobile development, and a full API reference.

## Template License

This project is using a starter template from Marcus Ng with his open sourced repository, together with his youtube tutorial

Details of the original resources:
- [GitHub Account](https://github.com/MarcusNg/flutter_travel_ui)
- [Project Repository](https://github.com/MarcusNg/flutter_travel_ui)
- [Youtube Tutorial](https://www.youtube.com/watch?v=CSa6Ocyog4U)
